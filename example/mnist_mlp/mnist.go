package main

import (
	"flag"
	"math"
	"math/rand"

	"gitlab.com/akita/dnn/training/optimization"

	"gitlab.com/akita/dnn/dataset/mnist"

	"gitlab.com/akita/dnn/layers"
	"gitlab.com/akita/dnn/training"
)

func main() {
	flag.Parse()
	rand.Seed(1)

	network := training.Network{
		Layers: []layers.Layer{
			layers.NewFullyConnectedLayer(784, 256),
			layers.NewReluLayer(),
			layers.NewFullyConnectedLayer(256, 100),
			layers.NewReluLayer(),
			layers.NewFullyConnectedLayer(100, 100),
			layers.NewReluLayer(),
			layers.NewFullyConnectedLayer(100, 10),
		},
	}
	trainer := training.Trainer{
		DataSource: mnist.NewTrainingDataSource(),
		Network:    network,
		LossFunc:   training.SoftmaxCrossEntropy{},
		//OptimizationAlg: optimization.NewSGD(0.03),
		//OptimizationAlg: optimization.NewMomentum(0.1, 0.9),
		//OptimizationAlg: optimization.NewRMSProp(0.003),
		OptimizationAlg: optimization.NewAdam(0.001),
		Tester: &training.Tester{
			DataSource: mnist.NewTestDataSource(),
			Network:    network,
			BatchSize:  math.MaxInt32,
		},
		Epoch:     1000,
		BatchSize: 128,
	}

	for _, l := range network.Layers {
		l.Randomize()
	}

	trainer.Train()
}
