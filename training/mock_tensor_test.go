// Code generated by MockGen. DO NOT EDIT.
// Source: gitlab.com/akita/dnn/tensor (interfaces: Tensor,Vector)

package training

import (
	gomock "github.com/golang/mock/gomock"
	tensor "gitlab.com/akita/dnn/tensor"
	reflect "reflect"
)

// MockTensor is a mock of Tensor interface
type MockTensor struct {
	ctrl     *gomock.Controller
	recorder *MockTensorMockRecorder
}

// MockTensorMockRecorder is the mock recorder for MockTensor
type MockTensorMockRecorder struct {
	mock *MockTensor
}

// NewMockTensor creates a new mock instance
func NewMockTensor(ctrl *gomock.Controller) *MockTensor {
	mock := &MockTensor{ctrl: ctrl}
	mock.recorder = &MockTensorMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockTensor) EXPECT() *MockTensorMockRecorder {
	return m.recorder
}

// Init mocks base method
func (m *MockTensor) Init(arg0 []float64, arg1 []int) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Init", arg0, arg1)
}

// Init indicates an expected call of Init
func (mr *MockTensorMockRecorder) Init(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Init", reflect.TypeOf((*MockTensor)(nil).Init), arg0, arg1)
}

// Size mocks base method
func (m *MockTensor) Size() []int {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Size")
	ret0, _ := ret[0].([]int)
	return ret0
}

// Size indicates an expected call of Size
func (mr *MockTensorMockRecorder) Size() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Size", reflect.TypeOf((*MockTensor)(nil).Size))
}

// Vector mocks base method
func (m *MockTensor) Vector() []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Vector")
	ret0, _ := ret[0].([]float64)
	return ret0
}

// Vector indicates an expected call of Vector
func (mr *MockTensorMockRecorder) Vector() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Vector", reflect.TypeOf((*MockTensor)(nil).Vector))
}

// MockVector is a mock of Vector interface
type MockVector struct {
	ctrl     *gomock.Controller
	recorder *MockVectorMockRecorder
}

// MockVectorMockRecorder is the mock recorder for MockVector
type MockVectorMockRecorder struct {
	mock *MockVector
}

// NewMockVector creates a new mock instance
func NewMockVector(ctrl *gomock.Controller) *MockVector {
	mock := &MockVector{ctrl: ctrl}
	mock.recorder = &MockVectorMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockVector) EXPECT() *MockVectorMockRecorder {
	return m.recorder
}

// Add mocks base method
func (m *MockVector) Add(arg0 tensor.Vector) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Add", arg0)
}

// Add indicates an expected call of Add
func (mr *MockVectorMockRecorder) Add(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Add", reflect.TypeOf((*MockVector)(nil).Add), arg0)
}

// AddScalar mocks base method
func (m *MockVector) AddScalar(arg0 float64) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "AddScalar", arg0)
}

// AddScalar indicates an expected call of AddScalar
func (mr *MockVectorMockRecorder) AddScalar(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddScalar", reflect.TypeOf((*MockVector)(nil).AddScalar), arg0)
}

// Clone mocks base method
func (m *MockVector) Clone() tensor.Vector {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Clone")
	ret0, _ := ret[0].(tensor.Vector)
	return ret0
}

// Clone indicates an expected call of Clone
func (mr *MockVectorMockRecorder) Clone() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Clone", reflect.TypeOf((*MockVector)(nil).Clone))
}

// DivElemWise mocks base method
func (m *MockVector) DivElemWise(arg0 tensor.Vector) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "DivElemWise", arg0)
}

// DivElemWise indicates an expected call of DivElemWise
func (mr *MockVectorMockRecorder) DivElemWise(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DivElemWise", reflect.TypeOf((*MockVector)(nil).DivElemWise), arg0)
}

// MulElemWise mocks base method
func (m *MockVector) MulElemWise(arg0 tensor.Vector) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "MulElemWise", arg0)
}

// MulElemWise indicates an expected call of MulElemWise
func (mr *MockVectorMockRecorder) MulElemWise(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "MulElemWise", reflect.TypeOf((*MockVector)(nil).MulElemWise), arg0)
}

// PowerScalar mocks base method
func (m *MockVector) PowerScalar(arg0 float64) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "PowerScalar", arg0)
}

// PowerScalar indicates an expected call of PowerScalar
func (mr *MockVectorMockRecorder) PowerScalar(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "PowerScalar", reflect.TypeOf((*MockVector)(nil).PowerScalar), arg0)
}

// Raw mocks base method
func (m *MockVector) Raw() []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Raw")
	ret0, _ := ret[0].([]float64)
	return ret0
}

// Raw indicates an expected call of Raw
func (mr *MockVectorMockRecorder) Raw() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Raw", reflect.TypeOf((*MockVector)(nil).Raw))
}

// Scale mocks base method
func (m *MockVector) Scale(arg0 float64) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Scale", arg0)
}

// Scale indicates an expected call of Scale
func (mr *MockVectorMockRecorder) Scale(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Scale", reflect.TypeOf((*MockVector)(nil).Scale), arg0)
}

// ScaleAdd mocks base method
func (m *MockVector) ScaleAdd(arg0, arg1 float64, arg2 tensor.Vector) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "ScaleAdd", arg0, arg1, arg2)
}

// ScaleAdd indicates an expected call of ScaleAdd
func (mr *MockVectorMockRecorder) ScaleAdd(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ScaleAdd", reflect.TypeOf((*MockVector)(nil).ScaleAdd), arg0, arg1, arg2)
}
