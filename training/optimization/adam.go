package optimization

import (
	"gitlab.com/akita/dnn/layers"
	"gitlab.com/akita/dnn/tensor"
)

// Adam runs the Adam optimization algorithm.
type Adam struct {
	LearningRate  float64
	SmoothFactor1 float64
	SmoothFactor2 float64

	historyV map[layers.Layer]tensor.Vector
	historyS map[layers.Layer]tensor.Vector
}

// NewAdam creates a new Adam optimization algorithm.
func NewAdam(learningRate float64) *Adam {
	return &Adam{
		LearningRate:  learningRate,
		SmoothFactor1: 0.9,
		SmoothFactor2: 0.999,
		historyV:      make(map[layers.Layer]tensor.Vector),
		historyS:      make(map[layers.Layer]tensor.Vector),
	}
}

func (r *Adam) UpdateParameters(layer layers.Layer) {
	params := layer.Parameters()
	gradients := layer.Gradients()

	if params == nil || gradients == nil {
		return
	}

	v := r.historyV[layer]
	s, found := r.historyS[layer]
	if !found {
		v = gradients.Clone()
		r.historyV[layer] = v
		s = gradients.Clone()
		s.MulElemWise(s)
		r.historyS[layer] = s
	} else {
		v.ScaleAdd(r.SmoothFactor1, 1-r.SmoothFactor1, gradients)
		squaredGradients := gradients.Clone()
		squaredGradients.MulElemWise(squaredGradients)
		s.ScaleAdd(r.SmoothFactor2, 1-r.SmoothFactor2, squaredGradients)
	}

	temp := s.Clone()
	temp.PowerScalar(0.5)
	temp.AddScalar(1e-8)
	direction := v.Clone()
	direction.DivElemWise(temp)

	params.ScaleAdd(1, -1.0*r.LearningRate, direction)
}
